run:
	code src
	docker-compose up -d
	docker-compose run --rm artisan migrate
run_start:
	code src
	docker-compose up -d
	cp src\.env.example src\.env
	docker-compose run --rm composer install
	docker-compose run --rm artisan migrate
	docker-compose run --rm artisan passport:install
	docker-compose run --rm artisan passport:client --personal
	docker-compose run --rm artisan db:seed
	docker-compose run --rm npm install
	docker-compose run --rm npm audit fix
	docker-compose run --rm npm run watch

migrate_only:
	docker-compose run --rm artisan migrate
	docker-compose run --rm artisan db:seed
	docker-compose run --rm artisan passport:client --personal
migrate:
	docker-compose run --rm artisan migrate:refresh
	docker-compose run --rm artisan db:seed
	docker-compose run --rm artisan passport:client --personal

start:
	docker-compose up -d --build site
	docker-compose run --rm composer create-project laravel/laravel .
	docker-compose run --rm composer install
	docker-compose run --rm npm install
	docker-compose run --rm npm audit fix
	
clean:
	rm -rf ./src
	mkdir src

restart: 
	make clean
	make start