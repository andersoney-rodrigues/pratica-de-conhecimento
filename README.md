# Usando o docker para rodar o ambiente de desenvolvimento.

## dependencias do projeto.
Docker-composer instalado
ultilizario de configuração make
Workbantch pode ser de grande ajuda para depuração dos dados.

## Instrução de uso com Docker
Tendo estas ferramentas pode-se realizar o deploy local da aplicação usando o comando `make run_start` e durante o desenvolvimento usar o `make run` 
Dentro do sistema usa-se como login o email admin@admin.com com a senha password, facilmente modificavel no arquivo de seeds. Ou removido. 

Há seeds no sistema para que seja possivel validar do começo ao fim sem a necessidade de ter que cadastrar os dados manualmente.

Recomendo caso queira desenvolver não use o `docker-compose run --rm npm run watch` e use o npm na sua propria maquina, dentro da maquina docker o sincronismo não funciona apropriadamente com o watch.

## Executando testes:
Para rodar o teste use: 

    docker-compose run --rm artisan test

Note que para o teste de registro o email precisa ser mudado para um que não exista no banco de dados.
# Caso queira usar o proprio php e composer em sua maquina
## Dependencias
* PHP - 7.4
* PHP fpm  - 7.4
* composer - Versão atual
* npm - Versão atual
  
## Comandos para execução do ambiente.

    code src
    cp src\.env.example src\.env
Antes de continuar a execução se faz necessario que configure o arquivo .env para apontar ao banco de dados em sua maquina. Modifique as linhas abaixos no arquivo para melhor refletir o acesso a seu banco de dados.

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=homestead
    DB_USERNAME=homestead
    DB_PASSWORD=secret

Continuando...

    cd src
    composer install
    artisan migrate
    artisan passport:install
    artisan passport:client --personal
    artisan db:seed
    npm install
    npm run watch
Apos o uso destes comandos execute a aplicação usando `php artisan serve`
