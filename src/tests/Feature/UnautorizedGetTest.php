<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UnautorizedGetTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_sucess()
    {
        $data = ['email' => 'andersoney@gmail.com', 'password' => 'password'];
        $response = $this->post('/api/login', $data);
        $data=$response->getContent();
        $data=json_decode($data,true);
        $response->assertJson(['success' => true, 'message' => 'User login successfully.']);
        $response->assertStatus(200);
        $response = $this->get('/api/announcements',['Authorization'=>'Bearer '.$data['data']['token']]);
        $response->assertStatus(200);
    }
    public function test_get_err()
    {
        $response = $this->get('/api/announcements');
        $response->dump();
        $response->assertStatus(302);
    }
}
