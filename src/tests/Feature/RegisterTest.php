<?php

namespace Tests\Feature;

use Faker\Factory as Faker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    public function test_register_err()
    {
        $data = ['email' => 'andersoney@gmail.com', 'password' => 'password'];
        $response = $this->post('/api/register', $data);
        $response->assertJson(['success' => false, 'message' => "Validation Error."]);
        $response->assertStatus(404);
    }
    public function test_register_sucess()
    {
        $faker = Faker::create(User::class);
        $data = [
            'email' => $faker->freeEmail,
            'name' => 'User Tester',
            'password' => 'password',
            'c_password' => 'password',
        ];
        // echo ("Testando com as seguintes informações: \n");
        // print_r($data);
        // echo ("\n");
        $response = $this->post('/api/register', $data);
        $response->assertJson(['success' => true, 'message' => "User register successfully."]);
        $response->assertStatus(200);
    }
}
