<?php

namespace Tests\Feature;

use Tests\TestCase;

class LoginTest extends TestCase
{
    public function test_login_sucess()
    {
        $data = ['email' => 'andersoney@gmail.com', 'password' => 'password'];
        $response = $this->post('/api/login', $data);
        $response->assertJson(['success' => true, 'message' => 'User login successfully.']);
        $response->assertStatus(200);
    }
    public function test_login_err()
    {
        $data = ['email' => 'andersoney2@gmail.com', 'password' => 'password'];
        $response = $this->post('/api/login', $data);
        $response->assertJson(['success' => false, 'message' => "Email or password invalid!"]);
        $response->assertStatus(401);
    }

}
