<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => "Andersoney Rodrigues",
            'email' => 'andersoney@gmail.com',
            'password' => Hash::make('password'),
            'type' => "admin",
        ]);
        $faker = Faker::create(User::class);
        foreach (range(1, 499) as $index) {
            User::create([
                'name' => $faker->firstname,
                'email' => $faker->unique()->email,
                'password' => Hash::make('password'),
            ]);
        }
    }
}
