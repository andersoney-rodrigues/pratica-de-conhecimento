<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class AnnouncementsSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 50) as $index) {
            DB::table('announcements')->insert([
                'user_id' => 1,
                'title' => $faker->text(150),
                'content' => $faker->text(1500),
                'expiration_date' => now()->addDay(150),
            ]);
        }
    }
}
