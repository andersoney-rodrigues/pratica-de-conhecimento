<?php

use App\Http\Controllers\API\AnnouncementsController;
use App\Http\Controllers\API\DefaultController;
use App\Http\Controllers\API\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::post('login', [DefaultController::class, 'login']);
Route::post('register', [DefaultController::class, 'register']);
Route::get('/active_login', [DefaultController::class, 'active_login']);

Route::middleware(['auth:api'])->group(function () {
    Route::resource('users', UserController::class)->except([
        'create', 'edit', 'update', 'destroy',
    ]);
    Route::resource('announcements', AnnouncementsController::class)->except([]);
});
