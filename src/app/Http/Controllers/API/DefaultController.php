<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class DefaultController extends BaseController
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return $this->errorResponse('Validation Error.', $validator->errors());
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken(env('APP_KEY'))->accessToken;
        $success['name'] = $user->name;

        return $this->sucessResponse('User register successfully.', $success);
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $success['token'] = $user->createToken(env('APP_KEY'))->accessToken;
            $success['name'] = $user->name;
            return $this->sucessResponse(__('User login successfully.'), $success);
        } else {
            return $this->unauthorizedResponse(__('Unauthorised.'), __("Email or password invalid!"), ['error' => 'Unauthorised']);
        }
    }
    public function active_login(Request $request)
    {
        $response = ['logged' => Auth::guard('api')->check(), 'message' => __('The user remains active in the system'), "title" => __('Logged in user.')];
        return $this->sucessResponse(__('User check sucess'), $response);
    }
}
