<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{
    public function getLoggedUser()
    {
        return Auth::user();
    }
    public function sucessResponse($message, $data)
    {
        $response = [
            'success' => true,
            'data' => $data,
            'message' => $message,
        ];
        return response()->json($response, 200);
    }
    public function errorResponse($message, $data)
    {
        $response = [
            'success' => false,
            'data' => $data,
            'message' => $message,
        ];

        return response()->json($response, 404);
    }
    public function unauthorizedResponse($title, $message, $data)
    {
        $response = [
            'success' => false,
            'data' => $data,
            'title' => $title,
            'message' => $message,
        ];

        return response()->json($response, 401);
    }
}
