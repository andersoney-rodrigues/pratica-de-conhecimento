<?php

namespace App\Http\Controllers\API;

use App\Models\Announcements;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Validator;

class AnnouncementsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Paginator::useBootstrap();
        if ($this->is_admin_user()) {
            return Announcements::where('status', 'active')->paginate(env('QTD_PAGINATE'));
        } else {
            return Announcements::where('user_id', Auth::user()->id)->paginate(env('QTD_PAGINATE'));
        }
    }
    private function is_admin_user()
    {
        return Auth::user()->type == 'admin';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
            'start_date' => 'required',
            'expiration_date' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorResponse('Validation Error.', $validator->errors());
        }
        $input = $request->all();
        $announcements = Announcements::create($input);
        return $this->sucessResponse(__("Sucess on create"), $announcements);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $announcement = Announcements::where('status', 'active')->find($id);
        if ($announcement && $announcement->status=='active') {
            return $this->sucessResponse(__('Sucess on Find'), $announcement);
        } else {
            return $this->errorResponse(__('Error on Find'), $announcement);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $announcement = Announcements::where('status', 'active')->find($id);
        if (!$announcement) {
            $this->errorResponse(__('Error on Find'), $announcement);
            return;
        }
        $dado = $request->all();
        $validator = Validator::make($dado, [
            'title' => '',
            'content' => '',
            'start_date' => '',
            'expiration_date' => '',
        ]);
        if ($validator->fails()) {
            return $this->errorResponse('Validation Error.', $validator->errors());
        }
        $announcement->update($dado);
        return $this->sucessResponse(__('Sucess on update'), $announcement);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $announcement = Announcements::find($id);
        if ($announcement) {
            // $announcement->delete();
            $announcement->status = 'deleted';
            $announcement->update();
            return $this->sucessResponse(__('Sucess on delete'), $announcement);
        } else {
            return $this->errorResponse(__('Error on delete'), $announcement);
        }
    }
}
