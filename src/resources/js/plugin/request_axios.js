import axios from "axios"
import Swal from 'sweetalert2'
export default {

    install(Vue) {
        console.log('Installed Axios')
        axios.interceptors.request.use(request => {
            $('#loading_modal').modal('show');
            let token = localStorage.getItem('token');
            if (request.url == "/api/login") {
                return request;
            } else {
                if (token == null) {
                    window.location.href = "http://localhost:83/login";
                } else {
                    request.headers['Authorization'] = 'Bearer ' + token;
                }
                return request;
            }
        });
        axios.interceptors.response.use(res => {
            $('.modal').modal('hide');
            return res;
        },
            err => {
                $('.modal').modal('hide');
                // Swal.fire({
                //     title: err.response.data.title,
                //     text: err.response.data.message,
                //     icon: 'error',
                //     confirmButtonText: 'Ok'
                // })
                throw new Error(err.response.data.message);
            });
        Vue.prototype.$get = function (url) {
            return axios.get(url)
        }
        Vue.prototype.$post = function (url, data) {
            return axios.post(url, data)
        }
        Vue.prototype.$put = function (url) {
            return axios.put(url, data)
        }
        Vue.prototype.$delete = function (url) {
            axios.get("/api/active_login")
                .then((response) => {
                    console.log(response.data.data.logged);
                    if (!response.data.data.logged) {
                        window.location.replace("/login");
                    }
                })
                .catch((err) => {
                });
            return axios.delete(url)
        }
        Vue.prototype.$axios = axios;
    }
}
