import axios from "axios"
import Swal from 'sweetalert2'
export default {

    install(Vue) {
        Vue.component('layout', require('../components/Layout/Layout.vue').default);
        Vue.component('title-page', require('../components/GlobalComponents/Title.vue').default);
        Vue.component('title-left', require('../components/GlobalComponents/TitleLeft.vue').default);
        Vue.component('alert', require('../components/GlobalComponents/Alert.vue').default);
        Vue.component('alert-manager', require('../components/GlobalComponents/AlertManager.vue').default);
        Vue.component('pagination', require('laravel-vue-pagination'));
    }
}
