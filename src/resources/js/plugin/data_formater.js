var moment = require('moment');
export default {

    install(Vue) {
        console.log('Installed Data formater')
        Vue.prototype.$dataFormat = (data_string) => {
            const d = new Date(data_string);
            return moment(d).format("YYYY/MM/DD hh:mm");
        }
        Vue.prototype.$dataFormatToDatatime_Local = (data_string) => {
            const d = new Date(data_string);
            return moment(d).format("YYYY-MM-DDTHH:mm");
        }
    }
}
