import axios from "axios"
import Swal from 'sweetalert2'
export default {

    install(Vue) {
        console.log('Installed Alert Components');
        Vue.prototype.$alert = function (data) {
            Swal.fire(data);
        }
    }
}
