/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');
require('sweetalert2');
require('inputmask');
window.Vue = require('vue').default;
import VueRouter from 'vue-router'
import http_plugin from './plugin/request_axios'
import components from './plugin/components'
import data_formater from './plugin/data_formater'
import router from './Vue/route'


// Vue.component('login-component', require("./components/LayoutLogin.vue").default);
Vue.component('dashboard-component', require("./components/Dashboard.vue").default);

Vue.use(VueRouter)
Vue.use(http_plugin)
Vue.use(components)
Vue.use(data_formater)


const app = new Vue({
    el: '#app',
    router
});
$(function () {
    $('body').tooltip({ trigger: "hover", selector: '[data-toggle=tooltip]' });
});
