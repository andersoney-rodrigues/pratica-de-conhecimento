import axios from 'axios';
import VueRouter from 'vue-router';
const ListComponent = require("../components/Routes/Announcements/list.vue").default
const CreateComponent = require("../components/Routes/Announcements/create.vue").default
const routes = [
    { path: '/', name: 'dashboard_home', component: ListComponent },
    { path: '/announcements', name: 'announcements_list', component: ListComponent },
    { path: '/announcements/create', name: 'announcements_create', component: CreateComponent },
    { path: '/announcements/:id', name: 'announcements_viewer', component: CreateComponent },
    { path: '/announcements/:id/edit', name: 'announcements_edit', component: CreateComponent }
]

const router = new VueRouter({
    routes
})
router.beforeEach((to, from, next) => {
    // console.log(to);
    // next();
    // return;
    //Isto foi necessario pois o tooltip estava ficando visivel ao ser redirecionado
    $('.tooltip.fade.bs-tooltip-top.show').remove();
    axios.get("/api/active_login")
        .then((response) => {
            console.log(response.data.data.logged);
            if (!response.data.data.logged) {
                window.location.replace("/login");
            }
        })
        .catch((err) => {
            // window.location.replace("/login");
        });
    next();
})
export default router;
