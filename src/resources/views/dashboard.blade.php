<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ env('APP_NAME') }}</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="antialiased">
    <div id="app">
        <dashboard-component></dashboard-component>
    </div>
</body>
<script type="text/javascript" src="{{ asset('js/app_dashboard.js') }}"></script>

</html>
